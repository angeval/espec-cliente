package com.itau.cliente.services;

import com.itau.cliente.exceptions.ClienteNotFoundException;
import com.itau.cliente.models.Cliente;
import com.itau.cliente.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;

    @ResponseStatus(HttpStatus.CREATED)
    public Cliente salvarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Optional<Cliente> buscarPorNumero(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()) {
            return clienteOptional;
        }
        //throw new ObjectNotFoundException(Cliente.class, "O cliente não foi encontrado");
        throw new ClienteNotFoundException();
    }

    public Cliente getById(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()) {
            return clienteOptional.get();
        }
        throw new ClienteNotFoundException();
    }

    public Iterable<Cliente> buscarTodosClientes() {
        Iterable<Cliente> clientes = clienteRepository.findAll();
        return clientes;
    }

    public Cliente atualizarCliente(Cliente cliente) throws ObjectNotFoundException {
        Cliente clienteDatabase = getById(cliente.getId());
        clienteDatabase.setName(cliente.getName());
        return clienteRepository.save(cliente);
    }
}