package com.itau.cliente.controller;

import com.itau.cliente.models.Cliente;
import com.itau.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @GetMapping
    @ResponseStatus(code=HttpStatus.OK)
    public Iterable<Cliente> buscarTodos() {
        return clienteService.buscarTodosClientes();
    }

    @GetMapping("/{id}")
    public Cliente buscarPorId(@PathVariable Integer id) {
        Cliente cliente =  clienteService.getById(id);
        return cliente;
    }

    @PostMapping
    @ResponseStatus(code=HttpStatus.CREATED)
    public Cliente salvar(@RequestBody @Valid Cliente cliente) {
        return clienteService.salvarCliente(cliente);
    }

}
